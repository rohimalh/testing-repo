package com.springcloud.product;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


import feign.RequestInterceptor;
import feign.RequestTemplate;

/**
 * 
 * @author Rohit
 *
 */
@SpringBootApplication

public class ProductAppApplication {
	
	/**
	 * 
	 */
	public static void main(String[] args) {
		SpringApplication.run(ProductAppApplication.class, args);
	}
	
	
}
