package com.springcloud.product.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
/**
 * 
 * @author Rohit
 *
 */
public class ProductController {
	/**
	 * <p>
	 * controller method for v1/product
	 * </p>
	 */
	@GetMapping("/v1/products")
	public String getAllProducts() {
		return "Hello World";
	}

	
}